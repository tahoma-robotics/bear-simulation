/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.lib.agent;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.Modifier;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;

public class Transformer implements ClassFileTransformer {

	private static final String HAL_CLASS_NAME = "edu/wpi/first/hal/HAL";
	private static final String MOT_CLASS_NAME = "com/ctre/phoenix/motorcontrol/can/MotControllerJNI";
	private static final String CTRJNI_CLASS_NAME = "com/ctre/phoenix/CTREJNIWrapper";
	private static final String PIGEON_CLASS_NAME = "com/ctre/phoenix/sensors/PigeonImuJNI";
	private static final String SOLENOID_CLASS_NAME = "edu/wpi/first/hal/SolenoidJNI";
	private static final String POWER_CLASS_NAME = "edu/wpi/first/hal/PowerJNI";
	
	private static final Map<String, String> halNativeMethods = new HashMap<>();
	private static final Map<String, String> motNativeMethods = new HashMap<>();
	private static final Map<String, String> pigNativeMethods = new HashMap<>();
	private static final Map<String, String> solNativeMethods = new HashMap<>();
	private static final Map<String, String> pwrNativeMethods = new HashMap<>();
	private static final Map<String, Map<String,String>> nativeClasses = new HashMap<>();
	
	static {
		halNativeMethods.put("initialize",
				"return org.tahomarobotics.sim.lib.hal.HAL.initialize($1, $2);");
		halNativeMethods.put("waitForDSData",
				"return org.tahomarobotics.sim.lib.hal.HAL.waitForDSData();");
		halNativeMethods.put("getMatchInfo",
				"{org.tahomarobotics.sim.lib.hal.HAL.MatchInfo info = org.tahomarobotics.sim.lib.hal.HAL.getMatchInfo(); $1.setData(info.eventName, info.gameSpecificMessage, info.matchNumber, info.replayNumber, info.matchType); return true;}");
		halNativeMethods.put("nativeGetAllianceStation",
				"return org.tahomarobotics.sim.lib.hal.HAL.nativeGetAllianceStation();");
		halNativeMethods.put("nativeGetControlWord",
				"return org.tahomarobotics.sim.lib.hal.HAL.nativeGetControlWord();");
		halNativeMethods.put("getJoystickAxes", 	
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickAxes($1, $2);");
		halNativeMethods.put("getJoystickPOVs", 				
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickPOVs($1, $2);");
		halNativeMethods.put("getJoystickButtons", 				
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickButtons($1, $2);");
		halNativeMethods.put("setJoystickOutputs", 				
				"return org.tahomarobotics.sim.lib.hal.HAL.setJoystickOutputs($1, $2, $3, $4);");
		halNativeMethods.put("getJoystickIsXbox", 				
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickIsXbox($1);");
		halNativeMethods.put("getJoystickType", 				
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickType($1);");
		halNativeMethods.put("getJoystickName", 				
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickName($1);");
		halNativeMethods.put("getJoystickAxisType", 			
				"return org.tahomarobotics.sim.lib.hal.HAL.getJoystickAxisType($1, $2);");
		halNativeMethods.put("sendError", 						
				"return org.tahomarobotics.sim.lib.hal.HAL.sendError($1, $2, $3, $4, $5, $6, $7);");
		
		motNativeMethods.put("Create", 							
				"return org.tahomarobotics.sim.lib.hal.MotController.Create($1);");
		motNativeMethods.put("Create2",
				"return org.tahomarobotics.sim.lib.hal.MotController.Create2($1, $2);");
		motNativeMethods.put("GetDeviceNumber",
				"return org.tahomarobotics.sim.lib.hal.MotController.GetDeviceNumber($1);");
		motNativeMethods.put("GetBaseID",
				"return org.tahomarobotics.sim.lib.hal.MotController.GetBaseID($1);");
		motNativeMethods.put("SetDemand", 						
				"return org.tahomarobotics.sim.lib.hal.MotController.SetDemand($1, org.tahomarobotics.sim.lib.hal.MotController.getMode($2), $3, $4);");
		motNativeMethods.put("SetInverted",
				"return org.tahomarobotics.sim.lib.hal.MotController.SetInverted($1, $2);");
		motNativeMethods.put("SetInverted_2",
				"return org.tahomarobotics.sim.lib.hal.MotController.SetInverted_2($1, $2);");
		motNativeMethods.put("ConfigSelectedFeedbackSensor",
				"return org.tahomarobotics.sim.lib.hal.MotController.ConfigSelectedFeedbackSensor($1, $2, $3, $4);");
		motNativeMethods.put("GetSelectedSensorVelocity", 		
				"return org.tahomarobotics.sim.lib.hal.MotController.GetSelectedSensorVelocity($1, $2);");
		motNativeMethods.put("GetSelectedSensorPosition", 		
				"return org.tahomarobotics.sim.lib.hal.MotController.GetSelectedSensorPosition($1, $2);");
		motNativeMethods.put("Set_4", 
				"return org.tahomarobotics.sim.lib.hal.MotController.Set_4($1, org.tahomarobotics.sim.lib.hal.MotController.getMode($2), $3, $4, $5);");
		motNativeMethods.put("SetNeutralMode",
				"return org.tahomarobotics.sim.lib.hal.MotController.SetNeutralMode($1, $2);");
		motNativeMethods.put("GetTemperature",
				"return 20.0;");
		motNativeMethods.put("GetStatorCurrent", "return 0.5;");
		
		pigNativeMethods.put("JNI_new_PigeonImu", 
				"return org.tahomarobotics.sim.lib.hal.PigeonIMU.JNI_new_PigeonImu($1);");
		pigNativeMethods.put("JNI_GetRawGyro", 
				"return org.tahomarobotics.sim.lib.hal.PigeonIMU.JNI_GetRawGyro($1, $2);");
		pigNativeMethods.put("JNI_GetFusedHeading",
				"return org.tahomarobotics.sim.lib.hal.PigeonIMU.JNI_GetFusedHeading($1, $2);");

		solNativeMethods.put("initializeSolenoidPort", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.initializeSolenoidPort($1);");
		
		solNativeMethods.put("checkSolenoidModule", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.checkSolenoidModule($1);");
		
		solNativeMethods.put("checkSolenoidChannel", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.checkSolenoidChannel($1);");
		
		solNativeMethods.put("freeSolenoidPort", 
				"org.tahomarobotics.sim.lib.hal.Solenoid.freeSolenoidPort($1);");
		
		solNativeMethods.put("setSolenoid", 
				"org.tahomarobotics.sim.lib.hal.Solenoid.setSolenoid($1, $2);");
		
		solNativeMethods.put("getSolenoid", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.getSolenoid($1);");
		
		solNativeMethods.put("getAllSolenoids", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.getAllSolenoids($1);");
		
		solNativeMethods.put("getPCMSolenoidBlackList", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.getPCMSolenoidBlackList($1);");
		
		solNativeMethods.put("getPCMSolenoidVoltageStickyFault", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.getPCMSolenoidVoltageStickyFault($1);");

		solNativeMethods.put("getPCMSolenoidVoltageFault", 
				"return org.tahomarobotics.sim.lib.hal.Solenoid.getPCMSolenoidVoltageFault($1);");

		solNativeMethods.put("clearAllPCMStickyFaults", 
				"org.tahomarobotics.sim.lib.hal.Solenoid.clearAllPCMStickyFaults($1);");

		solNativeMethods.put("setOneShotDuration", 
				"org.tahomarobotics.sim.lib.hal.Solenoid.setOneShotDuration($1, $2);");

		solNativeMethods.put("fireOneShot", 
				"org.tahomarobotics.sim.lib.hal.Solenoid.fireOneShot($1);");
		
		pwrNativeMethods.put("getVinVoltage", 
				"return org.tahomarobotics.sim.lib.hal.Power.getVinVoltage();");
		
		pwrNativeMethods.put("getVinCurrent", 
				"return org.tahomarobotics.sim.lib.hal.Power.getVinCurrent();");
		
		nativeClasses.put(HAL_CLASS_NAME, halNativeMethods);
		nativeClasses.put(MOT_CLASS_NAME, motNativeMethods);
		nativeClasses.put(PIGEON_CLASS_NAME, pigNativeMethods);
		nativeClasses.put(SOLENOID_CLASS_NAME, solNativeMethods);
		nativeClasses.put(POWER_CLASS_NAME, pwrNativeMethods);
	}
	
	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		
		try {
			Map<String, String> nativeMethods = nativeClasses.get(className);
			if (nativeMethods != null) {
				System.out.printf("transform class %s \n", className);
				ClassPool cp = ClassPool.getDefault();
				CtClass ctClass = cp.get(className.replace("/", "."));
				redirectNativeMethods(ctClass, nativeMethods);
				return ctClass.toBytecode();

			}
			
			if (className.contentEquals(CTRJNI_CLASS_NAME)) {
				ClassPool cp = ClassPool.getDefault();
				CtClass ctClass = cp.get(className.replace("/", "."));
				ctClass.getClassInitializer().setBody("libraryLoaded = true;");
				return ctClass.toBytecode();
			}
		} catch (NotFoundException | BadBytecode | CannotCompileException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private void redirectNativeMethods(CtClass ctClass, Map<String, String> nativeMethods) throws NotFoundException, BadBytecode, CannotCompileException {
		for(Entry<String, String> nativeMethod : nativeMethods.entrySet()) {
			CtMethod ctMethod = ctClass.getDeclaredMethod(nativeMethod.getKey());			
			System.out.println("Simulation intercepted method: " + ctMethod.getLongName());
			ctMethod.setModifiers(ctMethod.getModifiers() & ~Modifier.NATIVE);
			ctMethod.setBody(nativeMethod.getValue());
		}
	}
}
