/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.lib.agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class RobotBaseTransformer implements ClassFileTransformer {

	private static final String CLASS_NAME = "edu/wpi/first/wpilibj/RobotBase";
	
	//@formatter:off
	private static final String MAIN_BODY = 
"		try {"+
"			initializeHardwareConfiguration();"+
"			"+
"			HAL.report(tResourceType.kResourceType_Language, tInstances.kLanguage_Java);"+
"			"+
"			final File file = new File(\"C:\\Windows\\Temp\\frc_versions\\FRC_Lib_Version.ini\");"+
"           file.getParentFile().mkdirs();"+
"			if (file.exists()) {" +
"               file.delete();"+
"           }" +
"			file.createNewFile();"+
"			try (FileOutputStream output = new FileOutputStream(file)) {"+
"				output.write(\"Java \".getBytes());"+
"				output.write(WPILibVersion.Version.getBytes());"+
"			}"+
"			"+
"			Enumeration<URL> resources = RobotBase.class.getClassLoader().getResources(\"META-INF/MANIFEST.MF\");"+
"			while (resources != null && resources.hasMoreElements()) {"+
"			    Manifest manifest = new Manifest(resources.nextElement().openStream());"+
"				String robotName = manifest.getMainAttributes().getValue(\"Robot-Class\");"+
"               if (robotName != null) {"+
"			        System.out.println(\"********** Robot program starting **********\");"+
"			        RobotBase robot = (RobotBase) Class.forName(robotName).newInstance();"+
"			        robot.startCompetition();"+
"               }"+
"			}"+
"			"+
"		} catch (Exception e) {"+
"			DriverStation.reportError(\"Robots should not quit, but yours did!\", false);"+
"			e.printStackTrace();"+
"			System.exit(1);"+
"		}";
	//@formatter:on
	
	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {

		if (className.contentEquals(CLASS_NAME)) {

			try {

				System.out.println("Found: " + className);

				ClassPool cp = ClassPool.getDefault();
				CtClass ctClass = cp.get(className.replace("/", "."));

				CtMethod ctMethod = ctClass.getDeclaredMethod("main");
				System.out.println("Simulation intercepted method: " + ctMethod.getLongName());
				
				cp.importPackage("edu.wpi.first.wpilibj");
				cp.importPackage("edu.wpi.first.wpilibj.hal.HAL");
				cp.importPackage("java.util.Enumeration");
				cp.importPackage("java.util.jar.Manifest");
				cp.importPackage("edu.wpi.first.wpilibj.hal.FRCNetComm.tInstances");
				cp.importPackage("edu.wpi.first.wpilibj.hal.FRCNetComm.tResourceType");
				cp.importPackage("edu.wpi.first.wpilibj.hal.HAL");
				cp.importPackage("edu.wpi.first.wpilibj.util.WPILibVersion");
				cp.importPackage("java.io.File");
				cp.importPackage("java.io.FileOutputStream");
				cp.importPackage("java.io.IOException");
				cp.importPackage("java.net.URL");
				
				ctMethod.setBody(MAIN_BODY);

				return ctClass.toBytecode();

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}
	
}
