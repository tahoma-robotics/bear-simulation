package org.tahomarobotics.sim.lib.agent;

import javassist.*;
import javassist.bytecode.BadBytecode;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.HashMap;
import java.util.Map;

public class RevTransformer implements ClassFileTransformer {

    private static final String CAN_SPARK_MAX_CLASS_NAME = "com/revrobotics/CANSparkMax";
    private static final String CAN_SPARK_MAX_LOW_LEVEL_CLASS_NAME = "com/revrobotics/CANSparkMaxLowLevel";
    private static final String CAN_ENCODER_CLASS_NAME = "com/revrobotics/CANEncoder";
    private static final String REV_JNI_WRAPPER_CLASS_NAME = "com/revrobotics/jni/RevJNIWrapper";
    private static final String SPARK_MAX_JNI_CLASS_NAME = "com/revrobotics/jni/CANSparkMaxJNI";

    private static final HashMap<String, String> sparkMethods = new HashMap<>();
    private static final HashMap<String, String> sparkLowLevelMethods = new HashMap<>();
    private static final HashMap<String, String> canEncoderMethods = new HashMap<>();
    private static final Map<String, String> sparkMaxJniMethods = new HashMap<>();


    private static final Map<String, Map<String,String>> nativeClasses = new HashMap<>();

    static {
        sparkMethods.put("follow",
                "{ org.tahomarobotics.sim.lib.hal.MotController.setFollow(getDeviceId(), $2, $3); return null; } ");
        sparkMethods.put("setInverted",
                "return org.tahomarobotics.sim.lib.hal.MotController.invert(getDeviceId(), $1);");

        sparkLowLevelMethods.put("getFirmwareVersion",
                "return org.tahomarobotics.sim.lib.hal.MotController.getFirmwareVersion();");
        sparkLowLevelMethods.put("setpointCommand",
                "{ org.tahomarobotics.sim.lib.hal.MotController.setpointCommand(this.m_deviceID, $1); return null; }");

        canEncoderMethods.put("getVelocity",
                "return org.tahomarobotics.sim.lib.hal.MotController.getMaxVelocity(this.m_device.getDeviceId());");
        canEncoderMethods.put("getPosition",
                "return org.tahomarobotics.sim.lib.hal.MotController.getMaxPosition(this.m_device.getDeviceId());");

        sparkMaxJniMethods.put("c_SparkMax_GetAPIMajorRevision","return 1;");
        sparkMaxJniMethods.put("c_SparkMax_GetAPIMinorRevision","return 1;");
        sparkMaxJniMethods.put("c_SparkMax_GetAPIBuildRevision","return 1;");
        sparkMaxJniMethods.put("c_SparkMax_GetAPIVersion","return 1;");
        sparkMaxJniMethods.put("c_SparkMax_Create","return 1L;");
        sparkMaxJniMethods.put("c_SparkMax_SetIdleMode", "return 1;");
        sparkMaxJniMethods.put("c_SparkMax_SetSensorType", "return 0;");
        sparkMaxJniMethods.put("c_SparkMax_IsFollower", "return true;");
        sparkMaxJniMethods.put("c_SparkMax_GetOutputCurrent", "return 3.4f;");
        sparkMaxJniMethods.put("c_SparkMax_SetEncoderPosition", "return 0;");

        nativeClasses.put(CAN_SPARK_MAX_CLASS_NAME, sparkMethods);
        nativeClasses.put(CAN_SPARK_MAX_LOW_LEVEL_CLASS_NAME, sparkLowLevelMethods);
        nativeClasses.put(CAN_ENCODER_CLASS_NAME, canEncoderMethods);
        nativeClasses.put(SPARK_MAX_JNI_CLASS_NAME, sparkMaxJniMethods);
    }

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        try {
            Map<String, String> nativeMethods = nativeClasses.get(className);
            if (nativeMethods != null) {
                System.out.printf("transform class %s \n", className);
                ClassPool cp = ClassPool.getDefault();
                CtClass ctClass = cp.get(className.replace("/", "."));
                redirectNativeMethods(ctClass, nativeMethods);
                return ctClass.toBytecode();

            }

            if (className.contentEquals(REV_JNI_WRAPPER_CLASS_NAME)) {
                ClassPool cp = ClassPool.getDefault();
                CtClass ctClass = cp.get(className.replace("/", "."));
                ctClass.getClassInitializer().setBody("libraryLoaded = true;");
                return ctClass.toBytecode();
            }

        } catch (NotFoundException | BadBytecode | CannotCompileException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void redirectNativeMethods(CtClass ctClass, Map<String, String> nativeMethods) throws NotFoundException, BadBytecode, CannotCompileException {
        for(Map.Entry<String, String> nativeMethod : nativeMethods.entrySet()) {
            if (nativeMethod.getKey().equals("setpointCommand")) {
                CtMethod[] ctMethods = ctClass.getDeclaredMethods(nativeMethod.getKey());
                for (CtMethod method : ctMethods) {
                    method.setModifiers(method.getModifiers() & ~Modifier.NATIVE);
                    method.setBody(nativeMethod.getValue());
                    System.out.println("Simulation intercepted method: " + method.getLongName());
                }
            } else {
                CtMethod ctMethod = ctClass.getDeclaredMethod(nativeMethod.getKey());

                if(nativeMethod.getKey().equals("follow")) {
                    ctMethod = ctClass.getDeclaredMethod(nativeMethod.getKey(), new CtClass[] {
                            ClassPool.getDefault().get(CAN_SPARK_MAX_CLASS_NAME + "$ExternalFollower"),
                            CtClass.intType,
                            CtClass.booleanType});
                }

                ctMethod.setModifiers(ctMethod.getModifiers() & ~Modifier.NATIVE);
                ctMethod.setBody(nativeMethod.getValue());
                System.out.println("Simulation intercepted method: " + ctMethod.getLongName());
            }
        }
    }
}
