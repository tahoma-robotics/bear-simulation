/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.lib.hal;

import org.tahomarobotics.sim.model.SimRobot;

public class Solenoid {
	
	public static int initializeSolenoidPort(int halPortHandle) {
		return halPortHandle;
	}

	public static boolean checkSolenoidModule(int module) {
		return true;
	}

	public static boolean checkSolenoidChannel(int channel) {
		return true;
	}

	public static void freeSolenoidPort(int portHandle) {
	}

	public static void setSolenoid(int portHandle, boolean on) {
		int module = ( portHandle >> 8 ) & 0xFF;
		int channel = portHandle & 0xFF;
		SimRobot.getInstance().setSolenoid(module, channel, on);
	}

	public static boolean getSolenoid(int portHandle) {
		int module = ( portHandle >> 8 ) & 0xFF;
		int channel = portHandle & 0xFF;
		return SimRobot.getInstance().getSolenoid(module, channel);
	}

	public static int getAllSolenoids(int module) {
		throw new UnsupportedOperationException();
	}

	public static int getPCMSolenoidBlackList(int module) {
		throw new UnsupportedOperationException();
	}

	public static boolean getPCMSolenoidVoltageStickyFault(int module) {
		throw new UnsupportedOperationException();
	}

	public static boolean getPCMSolenoidVoltageFault(int module) {
		throw new UnsupportedOperationException();
	}

	public static void clearAllPCMStickyFaults(int module) {
		throw new UnsupportedOperationException();
	}

	public static void setOneShotDuration(int portHandle, long durationMS) {
		throw new UnsupportedOperationException();
	}

	public static void fireOneShot(int portHandle) {
		throw new UnsupportedOperationException();
	}
}
