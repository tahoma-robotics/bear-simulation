/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.lib.hal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.sim.model.SimRobot;

public class PigeonIMU {

	private static final Logger LOGGER = LoggerFactory.getLogger(PigeonIMU.class);
	
	public static long JNI_new_PigeonImu(int deviceNumber) {
		return deviceNumber;
	}
	
	public static int JNI_GetRawGyro(long handle, double [] xyz_dps) {
		
		if (xyz_dps == null || xyz_dps.length < 3) {
			LOGGER.error("Invalid arguments, must be double[3+]");
			return -2;
		}
		
		if (SimRobot.getInstance().getRawGyro((int) handle, xyz_dps)) {
			return 0;
		}
		
		LOGGER.error("Failed to find Pigeon with handle " + handle);
		return -2;
	}

	public static int JNI_GetFusedHeading(long handle, double [] fusionStatus) {
		fusionStatus[0] = SimRobot.getInstance().getHeading(handle);
		fusionStatus[1] = 1; // is fusing
		fusionStatus[2] = 1; // is valid
		return 0;
	}
}
