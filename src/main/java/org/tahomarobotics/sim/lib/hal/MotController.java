/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.lib.hal;

import org.tahomarobotics.sim.model.SimRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MotController {


    public enum Mode {
        PercentOutput, Follower;
    }

    public static Mode getMode(int value) {
        switch(value) {
            case 0: return Mode.PercentOutput;
            case 5: return Mode.Follower;
            default:
                throw new UnsupportedOperationException(String.format("Motor Controller mode %s not implemented\n", value));
        }
    }

	private static class Master {
		final List<Integer> slaves = new ArrayList<>();
	}
	
	private static final Map<Integer, Master> masters = new HashMap<>();	
	private static final Map<Integer, Integer> motorPolarity = new HashMap<>();
	
	private static int polarity(int device) {
		Integer polarity = motorPolarity.get(device);
		if (polarity == null) {
			polarity = 1;
			motorPolarity.put(device, polarity);
		}
		return polarity;
	}

	public static long Create(int baseArbId) {
		return baseArbId;
	}
	public static long Create2(int baseArbId, String model) {
		return baseArbId;
	}
	/**
	 * Returns the Device ID
	 * 
	 * @param handle - device handle
	 * @return Device number.
	 */
	public static int GetDeviceNumber(long handle) {
		return (int) (0xFFFF & handle);
	}
	public static int GetBaseID(long handle) {
		return (int) (0xFFFF & handle);
	}

	/**
	 * Sets the demand (output) of the motor controller.
	 * 
	 * @param handle - device handle
	 * @param mode - Control Mode of the Motor Controller
	 * @param demand0 - Primary Demand value
	 * @param demand1 - Secondary Demand value
	 **/
	public static void SetDemand(long handle, Mode mode, int demand0, int demand1) {
		int device = GetDeviceNumber(handle);
		
		if (mode == Mode.Follower) {
			int id = 0xFF & demand0;
			Master master = masters.get(id);
			if (master == null) {
				master = new Master();
				masters.put(id, master);
			}
			if (!master.slaves.contains(device)) {
				master.slaves.add(device);
			}
			
		} else {
			SimRobot.getInstance().setMotorControllerInput(device, mode, demand0 * polarity(device));
			Master master = masters.get(device);
			if (master != null) {
				for(int slave : master.slaves) {
					SimRobot.getInstance().setMotorControllerInput(slave, mode, demand0 * polarity(slave));		
				}
			}			
		}
	
	}
	
	/**
	 * Sets the demand (output) of the motor controller.
	 * 
	 * @param handle - device handle
	 * @param mode - motor controller mode
	 * @param demand0 - Primary Demand value
	 * @param demand1 - Secondary Demand value
	 * @param demand1Type - Secondary Demand type
	 */
	public static void Set_4(long handle, Mode mode, double demand0, double demand1, int demand1Type) {
		int device = GetDeviceNumber(handle);
		int demand0Int = (int) (1023 * demand0);
		if (mode == Mode.Follower) {
			int id = 0xFF & (int)demand0;
			Master master = masters.get(id);
			if (master == null) {
				master = new Master();
				masters.put(id, master);
			}
			if (!master.slaves.contains(device)) {
				master.slaves.add(device);
			}
			
		} else {
			SimRobot.getInstance().setMotorControllerInput(device, mode, demand0Int * polarity(device));
			Master master = masters.get(device);
			if (master != null) {
				for(int slave : master.slaves) {
					SimRobot.getInstance().setMotorControllerInput(slave, mode, demand0Int * polarity(slave));		
				}
			}			
		}
	
	}

	public static void SetNeutralMode(long handle, int neutralMode) {
		// TODO
	}

	/**
	 * Inverts the output of the motor controller. LEDs, sensor phase, and limit
	 * switches will also be inverted to match the new forward/reverse
	 * directions.
	 *
	 * @param handle - device handle
	 * @param invert
	 *            Invert state to set.
	 **/
	public static void SetInverted(long handle, boolean invert) {
		int device = GetDeviceNumber(handle);
		motorPolarity.put(device, invert ? -1 : 1);
	}

	/**
	 * Inverts the output of the motor controller. LEDs, sensor phase, and limit
	 * switches will also be inverted to match the new forward/reverse
	 * directions.
	 *
	 * @param handle - device handle
	 * @param invert
	 *            Invert state to set.
	 **/
	public static void SetInverted_2(long handle, int invert) {
		int device = GetDeviceNumber(handle);
		motorPolarity.put(device, invert > 0 ? -1 : 1);
	}

	/**
	 * Select the feedback device for the motor controller.
	 *
	 * @param handle - device handle
	 * @param feedbackDevice
	 *            Feedback Device to select.
	 * @param pidIdx - pid index
	 * @param timeoutMs
	 *            Timeout value in ms. @see #ConfigOpenLoopRamp
	 * @return Error Code generated by function. 0 indicates no error.
	 */
	public static int ConfigSelectedFeedbackSensor(long handle, int feedbackDevice, int pidIdx, int timeoutMs) {
		return 0;
	}

	/**
	 * Get the selected sensor velocity.
	 *
	 * @param handle - device handle
	 * @param pidIdx - pid index
	 * @return Velocity of selected sensor (in Raw Sensor Units per 100 ms).
	 */
	public static int GetSelectedSensorVelocity(long handle, int pidIdx) {
		int device = GetDeviceNumber(handle);
		return (int) (SimRobot.getInstance().getMotorControllerSensorVelocity(device) / 2 / Math.PI * 4096 / 10);
	}

	/**
	 * Get the selected sensor position.
	 *
	 * @param handle - device handle
	 * @param pidIdx - pid index
	 * @return Position of selected sensor (in Raw Sensor Units).
	 */
	public static int GetSelectedSensorPosition(long handle, int pidIdx) {
		int device = GetDeviceNumber(handle);
		return (int) (SimRobot.getInstance().getMotorControllerSensorPosition(device) / 2 / Math.PI * 4096);
	}

	//----------- Spark MAX Controllers
	public static int getFirmwareVersion(){
		return 20462046;
	}

	public static void setpointCommand(int deviceID, double value){
		SimRobot.getInstance().setMotorControllerInput(deviceID, Mode.PercentOutput, (int) (value * 1023)  * polarity(deviceID));
		Master master = masters.get(deviceID);
		if(master != null){
			for(int slave: master.slaves){
				SimRobot.getInstance().setMotorControllerInput(slave, Mode.PercentOutput, (int) (value * 1023) * polarity(slave));
			}
		}
	}

	public static void setFollow(int deviceID, int followingID, boolean inverted){
		Master master = masters.get(followingID);
		if(master == null){
			master = new Master();
			masters.put(followingID, master);
		}
		if(!master.slaves.contains(deviceID)){
			master.slaves.add(deviceID);
			invert(deviceID, inverted);
		}
	}

	public static void invert(int deviceID, boolean invert){
		int inverted = motorPolarity.getOrDefault(deviceID, 1);
		motorPolarity.put(deviceID, inverted * (invert ? -1 : 1));
	}

	public static double getMaxVelocity(int deviceID){
		return SimRobot.getInstance().getMotorControllerSensorVelocity(deviceID) / 2 / Math.PI * 60 * motorPolarity.getOrDefault(deviceID, 1);
	}

	public static double  getMaxPosition(int deviceID){
		return SimRobot.getInstance().getMotorControllerSensorPosition(deviceID) / 2 / Math.PI * motorPolarity.getOrDefault(deviceID, 1);
	}


}
