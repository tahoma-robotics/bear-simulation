/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.lib.hal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.sim.GameController;
import org.tahomarobotics.sim.RobotSimulation;

import java.nio.ByteBuffer;

public class HAL {

	public static class MatchInfo {
		public String eventName;
		public String gameSpecificMessage;
		public int matchNumber;
		public int replayNumber;
		public int matchType;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(HAL.class);

	private final static GameController gameController = GameController.getInstance();
	private final static RobotSimulation.State state = new RobotSimulation.State();

	private static boolean initialized = false;

	public static boolean initialize(int timeout, int mode) {

		if (initialized) {
			return true;
		}
		initialized = true;

		LOGGER.info("HAL.initialize(...)");
		// Wait for game controller to initialize
		GameController gameController = GameController.getInstance();
		try {
			for(int i = 0; i < 100 && !gameController.isInitialized(); i++) {
				Thread.sleep(10);

			};
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		return true;
	}

	private static MatchInfo info = new MatchInfo();

	public static MatchInfo getMatchInfo() {
		info.eventName = "Sim";
		info.gameSpecificMessage = "xyz";
		info.matchNumber = 1;
		info.replayNumber = 0;
		info.matchType = 0;
		return info;
	}

	public static int nativeGetAllianceStation() {
		return RobotSimulation.getInstance().getAllianceStation();
	}

	public static void waitForDSData() {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
		}
	}
	
	public static int nativeGetControlWord() {
		RobotSimulation.getInstance().getControlWord(state);
		// (word & 1) != 0, ((word >> 1) & 1) != 0, ((word >> 2) & 1) != 0, ((word >> 3) & 1) != 0, ((word >> 4) & 1) != 0, ((word >> 5) & 1) != 0
		int controlWord = 
			( state.isEnabled() ? 0x01 : 0x0 ) |
			( state.isAutonomous() ? 0x02 : 0x0 ) |
			( state.isTest() ? 0x04 : 0x0 ) |
			( state.isEmergencyStop() ? 0x08 : 0x0 ) |
			( state.isFmsAttached() ? 0x10 : 0x0 ) |
			( state.isDsAttached() ? 0x20 : 0x0 );
		
		return controlWord;
	}

	public static short getJoystickAxes(byte joystickNum, float[] axesArray) {
		return gameController.getJoystickAxes(joystickNum, axesArray);
	}

	public static short getJoystickPOVs(byte joystickNum, short[] povsArray) {
		return gameController.getJoystickPOVs(joystickNum, povsArray);
	}

	public static int getJoystickButtons(byte joystickNum, ByteBuffer count) {
		return gameController.getJoystickButtons(joystickNum, count);
	}

	public static int setJoystickOutputs(byte joystickNum, int outputs, short leftRumble, short rightRumble) {
		LOGGER.error("Joystick Rumble not simulated");
		return 0;
	}

	public static int getJoystickIsXbox(byte joystickNum) {
		return gameController.getJoystickIsXbox(joystickNum);
	}

	public static int getJoystickType(byte joystickNum) {
		return 20; // Joystick
	}

	public static String getJoystickName(byte joystickNum) {
		return gameController.getJoystickName(joystickNum);
	}

	public static int getJoystickAxisType(byte joystickNum, byte axis) {
		return 0;
	}

	public static int sendError(boolean isError, int errorCode, boolean isLVCode, String details, String location, String callStack, boolean printMsg) {
		if (isError) {
			LOGGER.error(details + "\n" + callStack);
		} else {
			LOGGER.info(details + "\n" + callStack);
		}
		return 0;
	}
}
