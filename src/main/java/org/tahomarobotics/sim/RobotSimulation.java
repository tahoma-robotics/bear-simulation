/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RobotSimulation {

	private static final Logger LOGGER = LoggerFactory.getLogger(RobotSimulation.class);
	
	private static final RobotSimulation instance = new RobotSimulation();

	public int getAllianceStation() {
		return state.allianceStation;
	}

	public void setAllianceStation(int allianceStation) {
		state.allianceStation = allianceStation;
	}

	public static class State {
		
		private boolean enabled;
		
		private boolean autonomous;
		
		private boolean test;

		private int allianceStation = 0;
		
		public boolean isEnabled() {
			return enabled;
		}
		public boolean isAutonomous() {
			return autonomous;
		}
		public boolean isTest() {
			return test;
		}
		
		public void set(boolean enabled, boolean autonomous, boolean test) {
			this.enabled = enabled;
			this.autonomous = autonomous;
			this.test = test;
		}
		
		public void set(State state) {
			this.enabled = state.enabled;
			this.autonomous = state.autonomous;
			this.test = state.test;
		}
		public boolean isEmergencyStop() {
			return false;
		}
		public boolean isFmsAttached() {
			return false;
		}
		public boolean isDsAttached() {
			return true;
		}


	}
	
	public final State state = new State();

	public static RobotSimulation getInstance() {
		return instance;
	}
	
	private RobotSimulation() {
		new Thread() {
			public void run() {
				DriverStationControls.main();

				LOGGER.info("exiting");
				System.exit(0);
			}
		}.start();

	}
	
	public void getControlWord(State state) {		
		synchronized(this.state) {
			state.set(this.state);
		}
	}
}
