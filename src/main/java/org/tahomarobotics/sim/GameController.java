/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.glfw.GLFW;

public class GameController {

	private static GameController instance = new GameController();

	public static GameController getInstance() {
		return instance;
	}

	private volatile boolean initialized = false;

	public boolean isInitialized() {
		return initialized;
	}

	private GameController() {
		GLFW.glfwInit();
	}

	private void swap(float values[], int a, int b) {
		float temp = values[a];
		values[a] = values[b];
		values[b] = temp;		
	}
	public short getJoystickAxes(byte joystickNum, float[] axesArray) {
		FloatBuffer buffer = GLFW.glfwGetJoystickAxes(joystickNum);
		initialized = true;
		if (buffer == null) {
			return 0;
		}
		short count = 0;
		while (buffer.hasRemaining() && count < axesArray.length) {
			axesArray[count++] = buffer.get();
		}
		
		swap(axesArray, 2, 4);
		swap(axesArray, 3, 5);
		
		return count;
	}

	public short getJoystickPOVs(byte joystickNum, short[] povsArray) {		
		ByteBuffer hats = GLFW.glfwGetJoystickHats(joystickNum);
		if (hats == null) {
			return 0;
		}
		short count = 0;
		while (hats.hasRemaining()) {
			switch(hats.get()) {
			case GLFW.GLFW_HAT_CENTERED:
				povsArray[count++] = -1;
				break;
			case GLFW.GLFW_HAT_UP:
				povsArray[count++] = 0;
				break;
			case GLFW.GLFW_HAT_RIGHT_UP:
				povsArray[count++] = 45;
				break;
			case GLFW.GLFW_HAT_RIGHT:
				povsArray[count++] = 90;
				break;
			case GLFW.GLFW_HAT_RIGHT_DOWN:
				povsArray[count++] = 135;
				break;
			case GLFW.GLFW_HAT_DOWN:
				povsArray[count++] = 180;
				break;
			case GLFW.GLFW_HAT_LEFT_DOWN:
				povsArray[count++] = 225;
				break;
			case GLFW.GLFW_HAT_LEFT:
				povsArray[count++] = 270;
				break;
			case GLFW.GLFW_HAT_LEFT_UP:
				povsArray[count++] = 315;
				break;				
			}
		}
		return count;
	}

	public int getJoystickButtons(byte joystickNum, ByteBuffer count) {
		ByteBuffer buttons = GLFW.glfwGetJoystickButtons(joystickNum);
		if (buttons == null) {
			return 0;
		}
		byte cnt = 0;
		int buttonInt = 0;
		
		while (buttons.hasRemaining()) {
			byte button = buttons.get();
			int bit = 0;
			if (button > 0) {
				bit = 1 << cnt;
			}
			buttonInt = buttonInt | bit;
			cnt++;
		}
		count.rewind();
		count.put(cnt);
		return buttonInt;
	}

	public int getJoystickIsXbox(byte joystickNum) {
		return getJoystickName(joystickNum).contains("Xbox") ? 1 : 0;
	}

	public String getJoystickName(byte joystickNum) {
		return GLFW.glfwGetJoystickName(joystickNum);
	}

}
