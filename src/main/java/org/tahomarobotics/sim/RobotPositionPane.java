/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.sim.model.ModelIF;
import org.tahomarobotics.sim.model.SimRobot;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;

import java.util.List;

public class RobotPositionPane extends Pane {

	public class RobotPosition {
		private final StringProperty description;
		private final DoubleProperty x;
		private final DoubleProperty y;
		private final DoubleProperty heading;

		RobotPosition(String description, double x, double y, double heading) {
			this.description = new SimpleStringProperty(description);
			this.x = new SimpleDoubleProperty(x);
			this.y = new SimpleDoubleProperty(y);
			this.heading = new SimpleDoubleProperty(heading);
		}

		public StringProperty getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description.set(description);
		}

		public DoubleProperty getXProperty() {
			return x;
		}

		public void setXProperty(double x) {
			this.x.set(x);
		}

		public DoubleProperty getYProperty() {
			return y;
		}

		public void setYProperty(double y) {
			this.x.set(y);
		}

		public DoubleProperty getHeadingProperty() {
			return heading;
		}

		public void setZProperty(double heading) {
			this.heading.set(heading);
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(RobotPositionPane.class);
	
	private TableView<RobotPosition> table = new TableView<>();
	private ObservableList<RobotPosition> robotPositions = FXCollections.observableArrayList();

	public RobotPositionPane() {

		TableColumn<RobotPosition, String> descriptionColumn = new TableColumn<>("Description");
		TableColumn<RobotPosition, Number> xColumn = new TableColumn<>("X");
		TableColumn<RobotPosition, Number> yColumn = new TableColumn<>("Y");
		TableColumn<RobotPosition, Number> headingColumn = new TableColumn<>("Heading");

		descriptionColumn.setCellValueFactory(cellData -> cellData.getValue().getDescription());
		xColumn.setCellValueFactory(cellData -> cellData.getValue().getXProperty());
		yColumn.setCellValueFactory(cellData -> cellData.getValue().getYProperty());
		headingColumn.setCellValueFactory(cellData -> cellData.getValue().getHeadingProperty());

		ObservableList<TableColumn<RobotPosition, ?>> columns = table.getColumns();
        columns.add(descriptionColumn);
        columns.add(xColumn);
        columns.add(yColumn);
        columns.add(headingColumn);

		table.setPrefWidth(450);
		table.setPrefHeight(300);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		table.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				RobotPosition robotPosition = robotPositions.get(newValue.intValue());
				
				double x = robotPosition.getXProperty().get();
				double y = robotPosition.getYProperty().get();
				double hdg = robotPosition.getHeadingProperty().get();
				
				LOGGER.info(String.format("Robot position reset to %6.1f, %6.1f, %6.1f", x, y, hdg));
				
				SimRobot.getInstance().setInitialPosition(x, y, hdg);
			}	
		});
		
		table.setItems(robotPositions);
		this.getChildren().add(table);
		
		table.getSelectionModel().select(0);

	}

	public void setPositionPresets(List<ModelIF.PositionPreset> presets) {

		robotPositions.clear();
		for(ModelIF.PositionPreset preset : presets) {
			robotPositions.add(new RobotPosition(preset.description, preset.x, preset.y, preset.hdg));
		}
	}
}
