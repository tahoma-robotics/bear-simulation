/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.messaging;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractMessageSocket {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	protected final Map<Integer, MessageHandler<? extends Message>> handlers = new HashMap<>();
	
	protected final BlockingQueue<Message> sendQueue = new LinkedBlockingQueue<>();
	
	private final int SYNC = 0x2046;

	protected volatile boolean connected = false;
	protected final String host;
	protected final int port;
	protected Socket socket;
	protected DataOutputStream sendStream = null;
	protected DataInputStream receiveStream = null;	


	public AbstractMessageSocket(String host, int port) {
		this.host = host;
		this.port = port;
		sendThread.start();
		receiveThread.start();
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public void sendMessage(Message message) {
		sendQueue.add(message);
	}
	
	public void registerHandler(int messageId, MessageHandler<? extends Message> handler) {
		handlers.put(messageId, handler);
	}

	protected abstract void reconnect();
	
	private Thread sendThread = new Thread() {
		@Override
		public void run() {
			
			ByteBuffer sendBuffer = ByteBuffer.allocate(Message.MAX_MESSAGE_SIZE);
			sendBuffer.order(ByteOrder.LITTLE_ENDIAN);
			
			while(true) {
				try {
					Message message = sendQueue.take();
					if (!connected) {
						LOGGER.error("Not connected, dropping message");
						continue;
					}
					sendBuffer.clear();
					sendBuffer.putInt(SYNC);
					sendBuffer.putInt(message.getMessageId());
					sendBuffer.putInt(message.getMessageSize());
					message.serialize(sendBuffer);
					int len = sendBuffer.position();
					byte dst[] = new byte[len];
					sendBuffer.rewind();
					sendBuffer.get(dst);
					sendStream.write(dst, 0, len);
				} catch (InterruptedException e) {
					// do nothing
				} catch (IOException e) {
					// do nothing
				}
			}
		}
	};

	private Thread receiveThread = new Thread() {
		@Override
		public void run() {
			
			byte buf[] = new byte[Message.MAX_MESSAGE_SIZE];
			ByteBuffer receiveBuffer = ByteBuffer.wrap(buf);
			receiveBuffer.order(ByteOrder.LITTLE_ENDIAN);
			
			while(true) {
				try {
					if (!connected) {
						reconnect();
					}
					// use reverseBytes to undo little endianess
					int sync = Integer.reverseBytes(receiveStream.readInt());
					if (sync == SYNC) {
						int messageId = Integer.reverseBytes(receiveStream.readInt());
						int len = Integer.reverseBytes(receiveStream.readInt());
						if (len > buf.length) {
							System.err.println("Message length is too large, id: " + messageId + " len = " + len);
							continue;
						}
						MessageHandler<? extends Message> handler = handlers.get(messageId);
						if (handler == null) {
							System.err.println("No message handler for id: " + messageId);
							continue;
						}
						
						receiveBuffer.clear();
						receiveStream.readFully(buf, 0, len);
						receiveBuffer.limit(len);
						
						handler.onMessage(receiveBuffer);
					}
				} catch (EOFException e ) {
					System.out.println("Connection lost w/EOF: " + e.getMessage());
					connected = false;
				} catch (SocketException e) {
					System.out.println("Connection lost: " + e.getMessage());
					connected = false;
				} catch (IOException e) {
					e.printStackTrace();
					connected = false;
				}
			}
		}
	};


}
