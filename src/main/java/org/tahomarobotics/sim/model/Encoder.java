package org.tahomarobotics.sim.model;

public class Encoder {

    private double velocity, position, ratio;

    public Encoder(double ratio){
        this.ratio = ratio;
    }

    public Encoder(){
        this(1);
    }

    public void setVelocity(double vel){
        velocity = vel * ratio;
    }

    public void setPosition(double pos){
        position = pos * ratio;
    }

    public double getVelocity(){
        return velocity;
    }

    public double getPosition(){
        return position;
    }

}
