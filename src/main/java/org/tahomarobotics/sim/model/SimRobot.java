/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.sim.RobotSimulation;
import org.tahomarobotics.sim.lib.hal.MotController;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class SimRobot {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimRobot.class);

	private static final SimRobot instance = new SimRobot();

	private final Map<Integer, Motor> motors = new HashMap<>();
	private final Map<Integer, Solenoid> solenoids = new HashMap<>();
	private final Map<Integer, IMU> imus = new HashMap<>();
	private final Map<Integer, Encoder> encoders = new HashMap<>();
	private final BatteryModel batteryModel = new BatteryModel();

	private final Timer loopTimer = new Timer();

	public static final SimRobot getInstance() {
		return instance;
	}

	private static final long DEFAULT_PERIOD = 5L;

	private SimRobot() {
		loopTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				update();
			}
		}, 0, DEFAULT_PERIOD);
	}

	public void initialize(ModelIF model) {
		clearRegistrations();
		model.initialize(this);
	}

	private void clearRegistrations() {
		motors.clear();
		solenoids.clear();
		imus.clear();
		encoders.clear();
	}

	public void registerMotor(int id, Motor motor) {
		motors.put(id, motor);
	}


	public void registerEncoder(int id, Encoder encoder) {
		encoders.put(id, encoder);
	}

	private int getSolenoidKey(int module, int channel) {
		return module * 1000 + channel;
	}

	public void registerSolenoid(int module, int channel, Solenoid solenoid) {
		solenoids.put(getSolenoidKey(module, channel), solenoid);
	}

	public void registerImu(int id, IMU imu) {
		imus.put(id, imu);
	}
	
	
	public synchronized void setSolenoid(int module, int channel, boolean state) {
		 ModelFactory.getInstance().getModel();
		
		Solenoid solenoid = solenoids.get(getSolenoidKey(module, channel));
		if (solenoid == null) {
			LOGGER.error("setSolenoid not implemented for module: " + module + " channel: " + channel);
			return;
		}

		solenoid.setExtended(state);
	}

	public synchronized boolean getSolenoid(int module, int channel) {
		 ModelFactory.getInstance().getModel();
		
		Solenoid solenoid = solenoids.get(getSolenoidKey(module, channel));
		if (solenoid == null) {
			LOGGER.error("setSolenoid not implemented for module: " + module + " channel: " + channel);
			return false;
		}

		return solenoid.isExtended();
	}

	public synchronized void setMotorControllerInput(int device, MotController.Mode mode, int value) {
		 ModelFactory.getInstance().getModel();
		
		Motor motor = motors.get(device);
		if (motor == null) {
			LOGGER.error("setMotorControllerInput not implemented for motor " + device);
			return;
		}

		if (!isSimEnabled()) {
			value = 0;
		}

		if (mode == MotController.Mode.PercentOutput) {
			double voltage = getBatteryVoltage() * value / 1023;
			motor.setVoltage(voltage);
			// LOGGER.debug("setMotorControllerInput output " + device + " = " + voltage);
		} else if (mode == MotController.Mode.Follower) {
			LOGGER.error("setMotorControllerInput follow mode ");

		} else {
			LOGGER.error("setMotorControllerInput not implemented for mode " + mode);
		}
	}

	public synchronized double getMotorControllerSensorPosition(int device) {
		 ModelFactory.getInstance().getModel();
		
		Encoder encoder = encoders.get(device);
		if (encoder == null) {
			LOGGER.error("getMotorControllerSensor not implemented for motor " + device);
			return 0;
		}
		return encoder.getPosition(); // radians
	}

	public synchronized double getMotorControllerSensorVelocity(int device) {
		 ModelFactory.getInstance().getModel();
		
		Encoder encoder = encoders.get(device);
		if (encoder == null) {
			LOGGER.error("getMotorControllerSensor not implemented for motor " + device);
			return 0;
		}
		return encoder.getVelocity();
	}

	public synchronized double getBatteryVoltage() {
		return batteryModel.getVoltage();
	}

	public double getBatteryCurrent() {
		return batteryModel.getCurrent();
	}

	public synchronized boolean isSimEnabled() {
		return RobotSimulation.getInstance().state.isEnabled();
	}

	private double time = System.currentTimeMillis() * 0.001;
	
	public synchronized void update() {
		double time = System.currentTimeMillis() * 0.001;
		double dT = time - this.time;
		this.time = time;

		ModelIF model =  ModelFactory.getInstance().getModel();
		if (model == null) {
			return;
		}
		model.update(time, dT);

		double current = 0;
		for (Motor motor : motors.values()) {
			current += Math.abs(motor.getCurrent());
		}
		batteryModel.setCurrent(current);
	}

	
	public synchronized void setInitialPosition(double x, double y, double heading) {
		
		// do not reset position if robot is enabled
		if (isSimEnabled()) {
			return;
		}
		
		ModelIF model =  ModelFactory.getInstance().getModel();
		model.resetPosition(x * 0.0254, y * 0.0254, Math.toRadians(heading));
		
	}

	private double[] angularVelocity = new double[3];
	
	public boolean getRawGyro(int handle, double[] xyz_dps) {
		 ModelFactory.getInstance().getModel();
		
		
		IMU imu = imus.get(handle);
		if (imu == null) {
			return false;
		}
		
		imu.getAngularVelocity(angularVelocity);
		for(int i = 0; i < 3; i++) {
			xyz_dps[i] = Math.toDegrees(angularVelocity[i]);
		}
		
		return true;
	}

	public double getHeading(long handle) {

		ModelFactory.getInstance().getModel();

		IMU imu = imus.get(handle);

		if (imu == null) {
			return -9999999.9;
		}

		return imu.getHeading();
	}
}
