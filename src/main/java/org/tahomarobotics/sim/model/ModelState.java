/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model;

public class ModelState {
	double time;
	double x;
	double y;
	double heading;
	double leftWheelSpeed;
	double rightWheelSpeed;
	double fwdVelocity;
	double rotVelocity;
	double liftPosition;
	double liftVelocity;
	double armPosition;
	double armVelocity;
	
	public double getLeftWheelSpeed() {
		return leftWheelSpeed;
	}
	
	public double getRightWheelSpeed() {
		return rightWheelSpeed;
	}
	
	public double getRotationalVelocity() {
		return rotVelocity;
	}
	
	public double getLiftPosition() {
		return liftPosition;
	}
	
	public double getLiftVelocity() {
		return liftVelocity;
	}
	
	public double getArmPosition() {
		return armPosition;
	}
	
	public double getArmVelocity() {
		return armVelocity;
	}
	
	public void get(ModelState state) {
		state.time = time;
		state.x = x;
		state.y = y;
		state.heading = heading;
		state.leftWheelSpeed = leftWheelSpeed;
		state.rightWheelSpeed = rightWheelSpeed;
		state.fwdVelocity = fwdVelocity;
		state.rotVelocity = rotVelocity;
		state.liftPosition = liftPosition;
		state.liftVelocity = liftVelocity;
		state.armPosition = armPosition;
		state.armVelocity = armVelocity;
		
	}

	@Override
	public String toString() {
		return String.format("Position: %6.3f %6.3f %6.3f Wheel: %6.3f %6.3f Lift: %6.3f %6.3f Arm: %6.3f %6.3f", 
				x, y, heading, leftWheelSpeed, rightWheelSpeed,
				liftPosition, liftVelocity, armPosition, armVelocity);
	}
	
	

}
