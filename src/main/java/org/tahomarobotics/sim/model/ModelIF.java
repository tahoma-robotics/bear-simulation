/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model;

import java.util.List;

/**
 * Model interface used by the simulation.  Each model should model the dynamics of the
 * physical robot based on it mechanical and electrical properties.  The result of the model is to
 * closely match the movement characteristics of the real robot.
 */
public interface ModelIF {

	/**
	 * Position preset which represents starting positions for the robot prior to enabling
	 */
	public class PositionPreset {

		// short description of the position preset
		public final String description;

		// x position
		public final double x;

		// y position
		public final double y;

		// heading
		public final double hdg;

		public PositionPreset(String description, double x, double y, double hdg) {
			this.description = description;
			this.x = x;
			this.y = y;
			this.hdg = hdg;
		}
	}

	/**
	 * Initialize is where effectors and sensors are registered.
	 *
	 * @param sim - simulation registration point
	 */
	void initialize(SimRobot sim);

	/**
	 * Returns if the model is connected to an external model.  This should return
	 * true if no external model exists.
	 *
	 * @return connected if true
	 */
	boolean isConnected();

	/**
	 * Resets the pose of the robot to the given values
	 *
	 * @param x - meters
	 * @param y - meters
	 * @param heading - radians
	 */
	void resetPosition(double x, double y, double heading);

	/**
	 * Enables the model depending on argument
	 * @param enabled - true enables the model
	 */
	void setEnabled(boolean enabled);

	/**
	 * Periodically called to update the model by integrating the values since
	 * the last time.
	 *
	 * @param time - time in seconds
	 * @param dT - delta time since last update
	 */
	void update(double time, double dT);

	/**
	 * Returns the name of this model.
	 *
	 * @return name - name of model
	 */
	String getName();

	/**
	 * Returns the pose of the robot as modeled
	 * @param pose - x, y, and heading (meters and radians)
	 */
	void getPose(double pose[]);

	/**
	 * Returns a list of preset position for repositioning the robot prior to
	 * enabling it.
	 *
	 * @return list of position presets
	 */
	List<PositionPreset> getPositionPresets();
}
