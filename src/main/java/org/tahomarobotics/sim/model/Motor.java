/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model;

public class Motor {

	private final double resistance; // ohms
	private final double kT;         // Nm / Amp
	private final double kV;         // rad/sec/volt
	private double velocity;         // rad/sec
	private double voltage;          // volt
	private double current;          // amp
	private double torque;           // Nm
	private double emf;              // volt
	
	private static final double RAD_PER_SEC = 2 * Math.PI / 60;
	
	public static Motor createCIM() {
		return new Motor(12.0, 131.0, 5330.0 * RAD_PER_SEC, 2.7, 2.41);
	}
	
	public static Motor createMiniCIM() {
		return new Motor(12.0, 89.0, 5840.0 * RAD_PER_SEC, 3.0, 1.4);
	}
	
	public static Motor create775Pro() {
		return new Motor(12.0, 134.0, 18730.0 * RAD_PER_SEC, 0.7, 0.71);
	}

	public static Motor createNeo() { return new Motor(12.0, 105, 5676 * RAD_PER_SEC, 1.8, 2.6); }

	public static Motor createNeo500() { return new Motor(12.0, 100, 11000 * RAD_PER_SEC, 1.4, 0.97); }

	public static Motor createFalcon500() {	return new Motor(12.0, 257, 6380 * RAD_PER_SEC, 1.5, 4.69); }

	private Motor(double specVoltage, double stallCurrent, double freeVelocity, double freeCurrent, double stallTorque) {
		resistance = specVoltage / stallCurrent;
		kT = stallTorque / (stallCurrent - freeCurrent);
		kV = freeVelocity / (specVoltage - resistance * freeCurrent);
	}
	
	public void setVoltage(double voltage) {
		this.voltage = voltage;
	}
	
	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	public double getTorque() {
		return torque;
	}
	
	public void update() {
		emf = velocity / kV;
		current = (voltage - emf) / resistance;
		torque = current * kT;
	}

	public double getVelocity() {
		return velocity;
	}

	@Override
	public String toString() {
		return String.format("v:%6.3f c:%6.3f e:%6.3f t:%6.3f", voltage, current, emf, torque);
	}

	public double getCurrent() {
		return current;
	}
	
	
}
