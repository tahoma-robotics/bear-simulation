/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(ModelFactory.class);
	
	private static final String MODEL_PROPERTY = "simulation.models";
	
	private final List<ModelIF> models = new ArrayList<>();

	private static ModelFactory instance = new ModelFactory();
	
	private ModelIF selectedModel = null;
	
	private ModelFactory() {
		
		String modelsProperty = System.getProperty(MODEL_PROPERTY);
		if (modelsProperty == null || modelsProperty.isEmpty()) {
			throw new RuntimeException(MODEL_PROPERTY + " is not set");
		}
		
		String models[] = modelsProperty.split(":");
		for (String model : models) {
			if (!model.isEmpty()) {
				LOGGER.info("Adding model: " + model);
				addModel(model);
			}
		}
	}
	
	private void addModel(String modelClassName) {
		try {
			Class<?> rawClazz = Class.forName(modelClassName);
			Object obj = rawClazz.getConstructor().newInstance();
			
			if (obj instanceof ModelIF) {
				ModelIF model = (ModelIF)obj;
				models.add(model);
				if (selectedModel == null) {
					setSelectedModel(model);
				}
			} else {
				LOGGER.error("Model " + modelClassName + " doesn't implement ModelIF");
			}
		} catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			LOGGER.error("Failed to create model " + modelClassName, e);
		}	
	}

	public static ModelFactory getInstance() {
		return instance;
	}
	
	public ModelIF getModel() {
		
//		boolean locked = RobotSimulation.getInstance().state.isEnabled();
//		
//		if (selectedModel == null || !locked) {
//			for(ModelIF model : models) {
//				if (model.isConnected()) {
//					setSelectedModel(model);
//					break;
//				}
//			}
//		}
		
		return selectedModel;
	}
	
	public void setSelectedModel(ModelIF selectedModel) {
		if (selectedModel == this.selectedModel) {
			return;
		}
		SimRobot.getInstance().initialize(selectedModel);
		this.selectedModel = selectedModel;
		
		for (ConnectionListener listener : listeners) {
			listener.onConnectionChange(selectedModel);
		}
	}
	
	private List<ConnectionListener> listeners = new ArrayList<>();
	
	public static interface ConnectionListener {
		void onConnectionChange(ModelIF model);
	}
	
	public void addConnectionListener(ConnectionListener listener) {
		listeners.add(listener);
	}

	public List<String> getModelNames() {
		List<String> modelNames = new ArrayList<>();
		for(ModelIF model : models) {
			modelNames.add(model.getName());
		}
		return modelNames;
	}
	
	public ModelIF getModelByName(String modelName) {
		for(ModelIF model : models) {
			if (modelName.equals(model.getName())) {
				return model;
			}
		}
		return null;
	}
}
