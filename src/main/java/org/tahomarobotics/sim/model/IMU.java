/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model;

public class IMU {
	
	private static final int X_AXIS = 0;
	private static final int Y_AXIS = 1;
	private static final int Z_AXIS = 2;
	
	private double[] angularVelocity = new double[3];
	private double heading;
	
	public void setZRotationalVelocity(double zRotationalVelocity) {
		this.angularVelocity[Z_AXIS] = zRotationalVelocity;
	}
	
	public void getAngularVelocity(double[] angularVelocity) {
		angularVelocity[X_AXIS] = this.angularVelocity[X_AXIS];
		angularVelocity[Y_AXIS] = this.angularVelocity[Y_AXIS];
		angularVelocity[Z_AXIS] = this.angularVelocity[Z_AXIS];
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}
	public double getHeading() {
		return heading;
	}
	
}
