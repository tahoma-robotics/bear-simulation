/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.sim.model.ModelFactory;
import org.tahomarobotics.sim.model.ModelFactory.ConnectionListener;
import org.tahomarobotics.sim.model.ModelIF;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.net.URL;
import java.text.Collator;
import java.util.*;
public class ControlPane extends VBox {

	private static final Logger LOGGER = LoggerFactory.getLogger(ControlPane.class);
	
	private static final String COUNTDOWN_KEY = "CountdownTime";

	private static final int COUNTDOWN_DEFUALT = 5;

	private static final String AUTONOMOUS_KEY = "AutonomousTime";
	
	private static final int AUTONOMOUS_DEFUALT = 15;

	private static final String DELAY_KEY = "DelayTime";

	private static final int DELAY_DEFUALT = 1;

	private static final String TELEOPERATED_KEY = "TeleoperatedTime";

	private static final int TELEOPERATED_DEFUALT = 135;

	private static final String ENDGAME_KEY = "EndgameTime";

	private static final int ENDGAME_DEFAULT = 30;

	private Timer practiceTimer;
	
	
	public ControlPane() {
		
		final ToggleGroup runMode = new ToggleGroup();
		final List<Control> runModeControls = new ArrayList<>();
		final List<Control> practiceSettings = new ArrayList<>();

		final RadioButton teleoperated = new RadioButton("TeleOperated");
		teleoperated.setToggleGroup(runMode);
		teleoperated.setSelected(true);
		runModeControls.add(teleoperated);
		teleoperated.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				enableControls(practiceSettings, false);
			}
		});

		final RadioButton autonomous = new RadioButton("Autonomous");
		autonomous.setToggleGroup(runMode);
		runModeControls.add(autonomous);
		autonomous.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				enableControls(practiceSettings, false);
			}
		});

		final RadioButton practice = new RadioButton("Practice");
		practice.setToggleGroup(runMode);
		runModeControls.add(practice);
		practice.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				enableControls(practiceSettings, true);
			}
		});

		final RadioButton test = new RadioButton("Test");
		test.setToggleGroup(runMode);
		runModeControls.add(test);
		test.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				enableControls(practiceSettings, false);
			}
		});

		final ToggleGroup enableDisable = new ToggleGroup();

		final ToggleButton enable = new ToggleButton("Enable");
		enable.setToggleGroup(enableDisable);
		

		final ToggleButton disable = new ToggleButton("Disable");
		disable.setToggleGroup(enableDisable);
		disable.setSelected(true);

		RobotSimulation sim = RobotSimulation.getInstance();

		final ComboBox<String> modelComboBox = new ComboBox<>();		

		// enable robot
		enable.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				synchronized (sim.state) {
					if (practice.isSelected()) {
						List<Control> all = new ArrayList<>();
						all.addAll(runModeControls);
						all.addAll(practiceSettings);
						startPractice(disable, all);
						enableControls(practiceSettings, false);
						
					} else {
						sim.state.set(true, autonomous.isSelected(), test.isSelected());
					}
					enableControls(runModeControls, false);
					modelComboBox.setDisable(true);
				}
			}
		});

		// disable robot
		disable.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				synchronized (sim.state) {
					sim.state.set(false, false, false);
					enableControls(runModeControls, true);
					modelComboBox.setDisable(false);
					if (practiceTimer != null) {
						practiceTimer.cancel();
						practiceTimer = null;
					}
				}
				modelComboBox.setDisable(false);
			}
		});

		VBox modeButtons = new VBox();

		modeButtons.getChildren().add(teleoperated);
		modeButtons.getChildren().add(autonomous);
		modeButtons.getChildren().add(practice);
		modeButtons.getChildren().add(test);
		modeButtons.setSpacing(10);

		HBox enableButtons = new HBox();
		enableButtons.getChildren().add(enable);
		enableButtons.getChildren().add(disable);
		enableButtons.setSpacing(10);

		int labelSpacing = 75;
		int textFieldSpacing = 50;

		Properties prop = SimProperties.getInstance().properties;

		int countdownTime = Integer.parseInt(prop.getProperty(COUNTDOWN_KEY, String.valueOf(COUNTDOWN_DEFUALT)));
		Label countDownLabel = new Label("Countdown");
		countDownLabel.setMinWidth(labelSpacing);
		countDownLabel.setAlignment(Pos.CENTER_RIGHT);
		TextField countDownField = new TextField();
		countDownField.setMaxWidth(textFieldSpacing);
		countDownField.setAlignment(Pos.CENTER_RIGHT);
		countDownField.setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(), countdownTime));
		countDownField.textProperty().addListener((observable, oldValue, newValue) -> {
			prop.setProperty(COUNTDOWN_KEY, newValue);
		});
		HBox countDownFields = new HBox();
		countDownFields.getChildren().add(countDownLabel);
		countDownFields.getChildren().add(countDownField);
		practiceSettings.add(countDownField);

		int autonoousTime = Integer.parseInt(prop.getProperty(AUTONOMOUS_KEY, String.valueOf(AUTONOMOUS_DEFUALT)));
		Label autonomousLabel = new Label("Autonomous");
		autonomousLabel.setMinWidth(labelSpacing);
		autonomousLabel.setAlignment(Pos.CENTER_RIGHT);
		TextField autonomousField = new TextField();
		autonomousField.setMaxWidth(textFieldSpacing);
		autonomousField.setAlignment(Pos.CENTER_RIGHT);
		autonomousField.setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(), autonoousTime));
		autonomousField.textProperty().addListener((observable, oldValue, newValue) -> {
			prop.setProperty(AUTONOMOUS_KEY, newValue);
		});
		HBox autonomousFields = new HBox();
		autonomousFields.getChildren().add(autonomousLabel);
		autonomousFields.getChildren().add(autonomousField);
		practiceSettings.add(autonomousField);

		int delayTime = Integer.parseInt(prop.getProperty(DELAY_KEY, String.valueOf(DELAY_DEFUALT)));
		Label delayLabel = new Label("Delay");
		delayLabel.setMinWidth(labelSpacing);
		delayLabel.setAlignment(Pos.CENTER_RIGHT);
		TextField delayField = new TextField();
		delayField.setMaxWidth(textFieldSpacing);
		delayField.setAlignment(Pos.CENTER_RIGHT);
		delayField.setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(), delayTime));
		delayField.textProperty().addListener((observable, oldValue, newValue) -> {
			prop.setProperty(DELAY_KEY, newValue);
		});
		HBox delayFields = new HBox();
		delayFields.getChildren().add(delayLabel);
		delayFields.getChildren().add(delayField);
		practiceSettings.add(delayField);

		int teleoperatedTime = Integer
				.parseInt(prop.getProperty(TELEOPERATED_KEY, String.valueOf(TELEOPERATED_DEFUALT)));
		Label teleoperatedLabel = new Label("Teleoperated");
		teleoperatedLabel.setMinWidth(labelSpacing);
		teleoperatedLabel.setAlignment(Pos.CENTER_RIGHT);
		TextField teleoperatedField = new TextField();
		teleoperatedField.setMaxWidth(textFieldSpacing);
		teleoperatedField.setAlignment(Pos.CENTER_RIGHT);
		teleoperatedField.setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(), teleoperatedTime));
		teleoperatedField.textProperty().addListener((observable, oldValue, newValue) -> {
			prop.setProperty(TELEOPERATED_KEY, newValue);
		});
		HBox teleoperatedFields = new HBox();
		teleoperatedFields.getChildren().add(teleoperatedLabel);
		teleoperatedFields.getChildren().add(teleoperatedField);
		practiceSettings.add(teleoperatedField);

		int endgameTime = Integer.parseInt(prop.getProperty(ENDGAME_KEY, String.valueOf(ENDGAME_DEFAULT)));
		Label endgameLabel = new Label("End Game");
		endgameLabel.setMinWidth(labelSpacing);
		endgameLabel.setAlignment(Pos.CENTER_RIGHT);
		TextField endgameField = new TextField();
		endgameField.setMaxWidth(textFieldSpacing);
		endgameField.setAlignment(Pos.CENTER_RIGHT);
		endgameField.setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(), endgameTime));
		endgameField.textProperty().addListener((observable, oldValue, newValue) -> {
			prop.setProperty(ENDGAME_KEY, newValue);
		});
		HBox endgameFields = new HBox();
		endgameFields.getChildren().add(endgameLabel);
		endgameFields.getChildren().add(endgameField);
		practiceSettings.add(endgameField);

		Button resetToDefault = new Button("Reset to Defaults");
		resetToDefault.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				countDownField.textProperty().set(String.valueOf(COUNTDOWN_DEFUALT));
				autonomousField.textProperty().set(String.valueOf(AUTONOMOUS_DEFUALT));
				delayField.textProperty().set(String.valueOf(DELAY_DEFUALT));
				teleoperatedField.textProperty().set(String.valueOf(TELEOPERATED_DEFUALT));
				endgameField.textProperty().set(String.valueOf(ENDGAME_DEFAULT));
			}
		});

		VBox practiceFields = new VBox();
		practiceFields.getChildren().add(countDownFields);
		practiceFields.getChildren().add(autonomousFields);
		practiceFields.getChildren().add(delayFields);
		practiceFields.getChildren().add(teleoperatedFields);
		practiceFields.getChildren().add(endgameFields);
		practiceFields.getChildren().add(resetToDefault);
		enableControls(practiceSettings, false);

		HBox selections = new HBox();
		selections.setSpacing(50);
		selections.getChildren().add(modeButtons);
		selections.getChildren().add(practiceFields);

		
		
		final ModelFactory modelFactory = ModelFactory.getInstance();
		List<String> modelNames = modelFactory.getModelNames();
		for (String modelName : modelNames) {
			modelComboBox.getItems().add(modelName);
		}
		modelComboBox.setValue(modelFactory.getModel().getName());
				
		final Label label = new Label("Model:");
		label.setTextAlignment(TextAlignment.CENTER);
		
		
		HBox status = new HBox();
		status.setSpacing(5);
		status.getChildren().addAll(label, modelComboBox);

		RobotPositionPane positionPane = new RobotPositionPane();

		modelFactory.addConnectionListener(new ConnectionListener() {
			@Override
			public void onConnectionChange(ModelIF model) {

				enable.setDisable(!model.isConnected());

				positionPane.setPositionPresets(model.getPositionPresets());
			}
		});
		enable.setDisable(!modelFactory.getModel().isConnected());
		positionPane.setPositionPresets(modelFactory.getModel().getPositionPresets());
	
		modelComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
            	ModelIF selectedModel = modelFactory.getModelByName(newValue);
				modelFactory.setSelectedModel(selectedModel);
            }    
        });

		HBox lowerCollection = new HBox();
		lowerCollection.setSpacing(50);
		lowerCollection.getChildren().add(enableButtons);
		lowerCollection.getChildren().add(status);

		Map<String, Integer> alliances = new HashMap<>();
		alliances.put("Red1", 0);
		alliances.put("Red2", 1);
		alliances.put("Red3", 2);
		alliances.put("Blue1",3);
		alliances.put("Blue2",4);
		alliances.put("Blue3",5);

		final ComboBox allianceSelection = new ComboBox<String>();
		final ObservableList<String> comboBoxItems = FXCollections.observableArrayList();
		comboBoxItems.addAll(alliances.keySet());
		SortedList<String> items = new SortedList(comboBoxItems);
		allianceSelection.setItems(items);
		allianceSelection.setValue("Red1");
		allianceSelection.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
				int station = alliances.get(newValue);
				RobotSimulation.getInstance().setAllianceStation(station);
			}
		});

		getChildren().add(selections);
		getChildren().add(lowerCollection);
		getChildren().add(allianceSelection);
		getChildren().add(positionPane);

		setSpacing(50);
		setPadding(new Insets(20, 10, 10, 20));

	}
	private static final int COUNTDOWN = 5;
	private static final int AUTONOMOUS = 15;
	private static final int DELAY = 1;
	private static final int TELEOP = 15;
	
	private enum PracticeState {COUNTDOWN, AUTO, DELAY, TELEOP, COMPLETE};
	
	protected void startPractice(final ToggleButton disable, final List<Control> runModeControls) {
		
		final RobotSimulation sim = RobotSimulation.getInstance();

		TimerTask practiceTask = new TimerTask() {

			PracticeState practiceState = PracticeState.COUNTDOWN;
			int count = 0;

			int autoStart = COUNTDOWN;
			int autoEnd = autoStart + AUTONOMOUS;
			int teleopStart = autoEnd + DELAY;
			int teleopEnd = teleopStart + TELEOP;
			
			@Override
			public void run() {
				synchronized(sim.state) {
					count++;
					
					switch(practiceState) {
					
					case COUNTDOWN:
						if (count >= autoStart) {
							practiceState = PracticeState.AUTO;
							sim.state.set(true, true, false);
							playSound(practiceState);
							LOGGER.info("start Auto");
						}
						break;
						
					case AUTO:
						if (count >= autoEnd) {
							practiceState = PracticeState.DELAY;
							sim.state.set(false, false, false);
							LOGGER.info("end Auto");
						}
						break;
					
					case DELAY:
						if (count >= teleopStart) {
							practiceState = PracticeState.TELEOP;
							sim.state.set(true, false, false);
							playSound(practiceState);
							LOGGER.info("start Teleop");
						}
						break;

					case TELEOP:
						if (count >= teleopEnd) {
							practiceState = PracticeState.COMPLETE;
							sim.state.set(false, false, false);
							playSound(practiceState);
							LOGGER.info("end Teleop");
						}
						break;
					
					case COMPLETE:
						disable.setSelected(true);
						enableControls(runModeControls, true);
						this.cancel();
						break;
					}
				}
			}
		};

		synchronized(sim.state) {
			sim.state.set(false, false, false);
			practiceTimer = new Timer();
			practiceTimer.scheduleAtFixedRate(practiceTask, 0, 1000);
		}
		
	}

	protected void playSound(PracticeState practiceState) {
		
		try {
			URL soundStreamUrl = null;
			switch (practiceState) {
			case AUTO:
				soundStreamUrl = DriverStationControls.class.getResource("/Start Auto_normalized.wav");
				break;

			case TELEOP:
				soundStreamUrl = DriverStationControls.class.getResource("/Start Teleop_normalized.wav");
				break;

			case COMPLETE:
				soundStreamUrl = DriverStationControls.class.getResource("/Match End_normalized.wav");
				break;

			default:
				break;

			}

			if (soundStreamUrl != null) {
				AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundStreamUrl);
				Clip clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				clip.start();
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void enableControls(List<Control> controls, boolean enabled) {
		for(Control control : controls) {
			control.setDisable(!enabled);
		}
	}
}
